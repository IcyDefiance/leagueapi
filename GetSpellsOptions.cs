﻿using System;

namespace LeagueApi
{
    [Flags]
    public enum GetSpellsOptions
    {
        None = 0,
        Cooldown = 1 << 0,
        CooldownBurn = 1 << 1,
        Cost = 1 << 2,
        CostBurn = 1 << 3,
        CostType = 1 << 4,
        Effect = 1 << 5,
        EffectBurn = 1 << 6,
        Image = 1 << 7,
        Key = 1 << 8,
        LevelTip = 1 << 9,
        MaxRank = 1 << 10,
        Modes = 1 << 11,
        Range = 1 << 12,
        RangeBurn = 1 << 13,
        Resource = 1 << 14,
        SanitizedDescription = 1 << 15,
        SanitizedTooltip = 1 << 16,
        Tooltip = 1 << 17,
        Vars = 1 << 18,
        All = 0x7FFFF
    }
}
