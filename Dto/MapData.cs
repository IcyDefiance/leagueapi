﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class MapData
    {
        public Dictionary<string, MapDetails> Data { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
    }
}
