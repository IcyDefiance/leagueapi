﻿namespace LeagueApi.Dto
{
    public class MasteryTreeItem
    {
        public int MasteryId { get; set; }
        public string PreReq { get; set; }
    }
}
