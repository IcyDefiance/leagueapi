﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class Champion
    {
        public List<string> AllyTips { get; set; }
        public string Blurb { get; set; }
        public List<string> EnemyTips { get; set; }
        public int Id { get; set; }
        public Image Image { get; set; }
        public Info Info { get; set; }
        public string Key { get; set; }
        public string Lore { get; set; }
        public string Name { get; set; }
        public string ParType { get; set; }
        public Passive Passive { get; set; }
        public List<Recommended> Recommended { get; set; }
        public List<Skin> Skins { get; set; }
        public List<ChampionSpell> Spells { get; set; }
        public Stats Stats { get; set; }
        public List<string> Tags { get; set; }
        public string Title { get; set; }
    }
}
