﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class ChampionList
    {
        public Dictionary<string, Champion> Data { get; set; }
        public string Format { get; set; }
        public Dictionary<string, string> Keys { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
    }
}
