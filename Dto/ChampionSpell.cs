﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class ChampionSpell
    {
        public List<Image> AltImages { get; set; }
        public List<double> Cooldown { get; set; }
        public string CooldownBurn { get; set; }
        public List<int> Cost { get; set; }
        public string CostBurn { get; set; }
        public string CostType { get; set; }
        public string Description { get; set; }
        public List<List<double>> Effect { get; set; }
        public List<string> EffectBurn { get; set; }
        public Image Image { get; set; }
        public string Key { get; set; }
        public LevelTip LevelTip { get; set; }
        public int MaxRank { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// This can be either a string or a list of int
        /// </summary>
        public object Range { get; set; }
        public string RangeBurn { get; set; }
        public string Resource { get; set; }
        public string SanitizedDescription { get; set; }
        public string SanitizedTooltip { get; set; }
        public string Tooltip { get; set; }
        public List<SpellVars> Vars { get; set; }
    }
}
