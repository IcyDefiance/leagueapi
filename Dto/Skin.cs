﻿namespace LeagueApi.Dto
{
    public class Skin
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Num { get; set; }
    }
}
