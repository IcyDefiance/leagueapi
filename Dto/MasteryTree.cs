﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class MasteryTree
    {
        public List<MasteryTreeList> Defense { get; set; }
        public List<MasteryTreeList> Offense { get; set; }
        public List<MasteryTreeList> Utility { get; set; }
    }
}
