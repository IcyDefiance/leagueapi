﻿namespace LeagueApi.Dto
{
    public class Gold
    {
        public int Base { get; set; }
        public bool Purchasable { get; set; }
        public int Sell { get; set; }
        public int Total { get; set; }
    }
}
