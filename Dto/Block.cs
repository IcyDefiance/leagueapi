﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class Block
    {
        public List<BlockItem> Items { get; set; }
        public bool RecMath { get; set; }
        public string Type { get; set; }
    }
}
