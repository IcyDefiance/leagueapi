﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class RuneList
    {
        public BasicData Basic { get; set; }
        public Dictionary<string, RuneData> Data { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
    }
}
