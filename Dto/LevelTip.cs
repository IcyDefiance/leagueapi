﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class LevelTip
    {
        public List<string> Effect { get; set; }
        public List<string> Label { get; set; }
    }
}
