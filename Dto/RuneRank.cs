﻿namespace LeagueApi.Dto
{
    public class RuneRank
    {
        public long Rank { get; set; }
        public long RuneId { get; set; }
    }
}
