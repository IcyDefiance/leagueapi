﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class MasteryTreeList
    {
        public List<MasteryTreeItem> MasteryTreeItems { get; set; }
    }
}
