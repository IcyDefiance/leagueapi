﻿namespace LeagueApi.Dto
{
    public class MasteryRank
    {
        public long MasteryId { get; set; }
        public long Rank { get; set; }
    }
}
