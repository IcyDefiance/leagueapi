﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class PlayerHistory
    {
        public List<MatchSummary> Matches { get; set; }
    }
}
