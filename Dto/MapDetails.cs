﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class MapDetails
    {
        public Image Image { get; set; }
        public long MapId { get; set; }
        public string MapName { get; set; }
        public List<long> UnpurchasableItemList { get; set; }
    }
}
