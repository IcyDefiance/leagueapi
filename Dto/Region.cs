﻿namespace LeagueApi.Dto
{
    public enum Region
    {
        BR,
        EUNE,
        EUW,
        KR,
        LAN,
        LAS,
        NA,
        OCE,
        RU,
        TR
    }
}
