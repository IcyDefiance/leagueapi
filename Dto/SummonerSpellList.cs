﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class SummonerSpellList
    {
        public Dictionary<string, SummonerSpell> Data { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
    }
}
