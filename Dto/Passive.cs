﻿namespace LeagueApi.Dto
{
    public class Passive
    {
        public string Description { get; set; }
        public Image Image { get; set; }
        public string Name { get; set; }
        public string SanitizedDescription { get; set; }
    }
}
