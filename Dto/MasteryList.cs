﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class MasteryList
    {
        public Dictionary<string, Mastery> Data { get; set; }
        public MasteryTree Tree { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
    }
}
