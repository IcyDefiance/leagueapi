﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class Participant
    {
        public int ChampionId { get; set; }
        public string HighestAchievedSeasonTier { get; set; }
        public List<MasteryRank> Masteries { get; set; }
        public int ParticipantId { get; set; }
        public List<RuneRank> Runes { get; set; }
        public int Spell1Id { get; set; }
        public int Spell2Id { get; set; }
        public ParticipantStats Stats { get; set; }
        public int TeamId { get; set; }
        public ParticipantTimeline Timeline { get; set; }
    }
}
