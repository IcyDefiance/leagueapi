﻿using System.Collections.Generic;

namespace LeagueApi.Dto
{
    public class Mastery
    {
        public List<string> Description { get; set; }
        public int Id { get; set; }
        public Image Image { get; set; }
        public string MasteryTree { get; set; }
        public string Name { get; set; }
        public string PreReq { get; set; }
        public int Ranks { get; set; }
        public List<string> SanitizedDescription { get; set; }
    }
}
