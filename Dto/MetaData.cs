﻿namespace LeagueApi.Dto
{
    public class MetaData
    {
        public bool IsRune { get; set; }
        public string Tier { get; set; }
        public string Type { get; set; }
    }
}
