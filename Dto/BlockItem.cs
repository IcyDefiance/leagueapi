﻿namespace LeagueApi.Dto
{
    public class BlockItem
    {
        public int Count { get; set; }
        public int Id { get; set; }
    }
}
