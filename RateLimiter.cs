﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;

namespace LeagueApi
{
    class RateLimiter
    {
        TimeSpan timeLimit;
        int requestLimit;

        public Queue<DateTime> requestTimes;
        Timer cleanupTimer;

        public RateLimiter(TimeSpan timeLimit, int requestLimit)
        {
            this.timeLimit = timeLimit;
            this.requestLimit = requestLimit;

            requestTimes = new Queue<DateTime>();
            cleanupTimer = new Timer();
            cleanupTimer.Elapsed += cleanupTimer_Elapsed;
        }

        public async Task WaitOnLimit()
        {
            while (requestTimes.Count >= requestLimit)
                await Task.Yield();

            requestTimes.Enqueue(DateTime.Now);
            Cleanup();
        }

        void Cleanup()
        {
            cleanupTimer.Stop();
            DateTime now = DateTime.Now;

            while (requestTimes.Count > 0 && now - requestTimes.Peek() >= timeLimit)
                requestTimes.Dequeue();

            if (requestTimes.Count > 0)
            {
                cleanupTimer.Interval = (timeLimit - (now - requestTimes.Peek())).TotalMilliseconds;
                cleanupTimer.Start();
            }
        }

        void cleanupTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Cleanup();
        }
    }
}
