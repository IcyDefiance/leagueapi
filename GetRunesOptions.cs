﻿using System;

namespace LeagueApi
{
    [Flags]
    public enum GetRunesOptions
    {
        None = 0,
        Basic = 1 << 0,
        Colloq = 1 << 1,
        ConsumeOnFull = 1 << 2,
        Consumed = 1 << 3,
        Depth = 1 << 4,
        From = 1 << 5,
        Gold = 1 << 6,
        HideFromAll = 1 << 7,
        Image = 1 << 8,
        InStore = 1 << 9,
        Into = 1 << 10,
        Maps = 1 << 11,
        RequiredChampion = 1 << 12,
        SanitizedDescription = 1 << 13,
        SpecialRecipe = 1 << 14,
        Stacks = 1 << 15,
        Stats = 1 << 16,
        Tags = 1 << 17,
        All = 0x3FFFF
    }
}
