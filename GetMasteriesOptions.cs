﻿using System;

namespace LeagueApi
{
    [Flags]
    public enum GetMasteriesOptions
    {
        None = 0,
        Image = 1 << 0,
        MasteryTree = 1 << 1,
        Prereq = 1 << 2,
        Ranks = 1 << 3,
        SanitizedDescription = 1 << 4,
        Tree = 1 << 5,
        All = 0x3F
    }
}
