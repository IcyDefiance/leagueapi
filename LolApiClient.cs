﻿using LeagueApi.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace LeagueApi
{
    public class LolApiClient
    {
        static RateLimiter rateLimit10Sec, rateLimit10Min;
        string apikey;
        Region region;

        public LolApiClient(string apikey, Region region)
        {
            if (rateLimit10Sec == null)
                rateLimit10Sec = new RateLimiter(TimeSpan.FromSeconds(10), 10);
            if (rateLimit10Min == null)
                rateLimit10Min = new RateLimiter(TimeSpan.FromMinutes(10), 500);

            this.apikey = apikey;
            this.region = region;
        }

        public async Task<ChampionList> GetChampions(GetChampionsOptions options = GetChampionsOptions.None)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/static-data/{0}/v1.2/champion",
                region.ToString().ToLower());

            var parameters = new Dictionary<string, string>();
            if (options != GetChampionsOptions.None)
                parameters["champData"] = options.ToString().Replace(" ", "").ToLower();

            return await GetDto<ChampionList>(url, parameters);
        }

        public async Task<MapData> GetMaps()
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/static-data/{0}/v1.2/map",
                region.ToString().ToLower());

            return await GetDto<MapData>(url);
        }

        public async Task<MasteryList> GetMasteries(GetMasteriesOptions options = GetMasteriesOptions.None)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/static-data/{0}/v1.2/mastery",
                region.ToString().ToLower());

            var parameters = new Dictionary<string, string>();
            if (options != GetMasteriesOptions.None)
            {
                parameters["masteryListData"] = string.Join(
                    ",",
                    options.ToString()
                        .Replace(" ", "")
                        .Split(',')
                        .Select(x => Char.ToLowerInvariant(x[0]) + x.Substring(1))
                );
            }

            return await GetDto<MasteryList>(url, parameters);
        }

        public async Task<RuneList> GetRunes(GetRunesOptions options = GetRunesOptions.None)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/static-data/{0}/v1.2/rune",
                region.ToString().ToLower());

            var parameters = new Dictionary<string, string>();
            if (options != GetRunesOptions.None)
            {
                parameters["runeListData"] = string.Join(
                    ",",
                    options.ToString()
                        .Replace(" ", "")
                        .Split(',')
                        .Select(x => Char.ToLowerInvariant(x[0]) + x.Substring(1))
                );
            }

            return await GetDto<RuneList>(url, parameters);
        }

        public async Task<SummonerSpellList> GetSpells(GetSpellsOptions options = GetSpellsOptions.None)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/static-data/{0}/v1.2/summoner-spell",
                region.ToString().ToLower());

            var parameters = new Dictionary<string, string>();
            if (options != GetSpellsOptions.None)
            {
                parameters["spellData"] = string.Join(
                    ",",
                    options.ToString()
                        .Replace(" ", "")
                        .Split(',')
                        .Select(x => Char.ToLowerInvariant(x[0]) + x.Substring(1))
                );
            }

            return await GetDto<SummonerSpellList>(url, parameters);
        }

        public async Task<PlayerHistory> GetMatchHistory(long summonerId, int beginIndex = 0, int endIndex = -1)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/{0}/v2.2/matchhistory/{1}",
                region.ToString().ToLower(),
                summonerId);

            var parameters = new Dictionary<string, string>();
            parameters["beginIndex"] = beginIndex.ToString();
            if (endIndex > -1)
                parameters["endIndex"] = endIndex.ToString();

            return await GetDto<PlayerHistory>(url, parameters);
        }

        public async Task<Summoner> GetSummoner(string name)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/{0}/v1.4/summoner/by-name/{1}",
                region.ToString().ToLower(),
                name);

            return (await GetDto<Dictionary<string, Summoner>>(url))[name];
        }

        public async Task<Dictionary<string, Summoner>> GetSummoners(List<string> names)
        {
            string url = string.Format("https://na.api.pvp.net/api/lol/{0}/v1.4/summoner/by-name/{1}",
                region.ToString().ToLower(),
                string.Join(",", names));

            return await GetDto<Dictionary<string, Summoner>>(url);
        }

        async Task<T> GetDto<T>(string url, Dictionary<string, string> parameters = null)
        {
            await rateLimit10Sec.WaitOnLimit();
            await rateLimit10Min.WaitOnLimit();

            var query = HttpUtility.ParseQueryString(string.Empty);
            query["api_key"] = apikey;
            if (parameters != null)
                foreach (var key in parameters.Keys)
                    query[key] = parameters[key];

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url + "?" + query.ToString());
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}
