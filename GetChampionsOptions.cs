﻿using System;

namespace LeagueApi
{
    [Flags]
    public enum GetChampionsOptions
    {
        None = 0,
        AllyTips = 1 << 0,
        AltImages = 1 << 1,
        Blurb = 1 << 2,
        EnemyTips = 1 << 3,
        Image = 1 << 4,
        Info = 1 << 5,
        Lore = 1 << 6,
        ParType = 1 << 7,
        Passive = 1 << 8,
        Recommended = 1 << 9,
        Skins = 1 << 10,
        Spells = 1 << 11,
        Stats = 1 << 12,
        Tags = 1 << 13,
        All = 0x3FFF
    }
}
